FROM uber/prototool:1.8.1

RUN apk --no-cache add grpc-java && \
    ln -s /usr/bin/protoc-gen-grpc-java /usr/bin/grpc_java_plugin
